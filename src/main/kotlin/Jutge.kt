import java.time.LocalDate
import java.util.Scanner

val sc = Scanner(System.`in`)
val problemes = listOf(
    Problema(
        "Prepara els triptics",
        "Benvingut a ITB, en aquest institut hem tingut alguns problemes d'organització i esperem que ens ajudis a solucionar-los\n " +
                "Per a imprimir els triptics informarius necesitem mostrar el nom complert de l'insitut",
        "No hi ha entrada.",
        "Però si  ens mostrarà el nom de l'institut: “Institut Tecnologic Barcelona”.",
        "",
        "Hello world!",
        "(no hi ha input)",
        "Institut Tecnologic Barcelona"
    ),
    Problema(
        "Alumnes nous",
        "Els triptics han funcionat massa bé, però l'institut no està preparat per a tants interesats en entrar\n" +
                "L'institut vol afegir dues aules més per a oferir més places, però necesitem saber cuantes estem oferint",
        "Hauràs de pasar la capacitat de cada aula",
        "I has de rebre el total d'alumnres que caben entre les dues aules",
        "4 8",
        "12",
        "32 37",
        "69"
    ),
    Problema(
        "Material escolar",
        "Com era d'esperar, les dues aules s'han omplert (F perls que no han pogut entrar) \n" +
                "Ara hem de comprar el paterial necesari per a cada alumne, es a dir, un ordinador, un monitor, un teclar i un ratolí",
        "Per l’entrada rebrà els alumnes del problema anterior",
        "Per la sortida imprimirà el total de material escolar que s'ha de comprar",
        "12",
        "48",
        "69",
        "276"
    ),

    Problema(
        "Esbarjo",
        "Amb els alumnes nous el pati exterior s'ha quedat petit així que el director vol ampliar-lo amb una petita pista de voley\n" +
                "Necesitem la teva ajuda per a calcular el area de terreny que necesitem per a ficar la pista",
        "Per l’entrada rebrà l'amplada i llargada de la pista de voley",
        "Per la sortida imprimirà el area que ocupa la pista",
        "2 3",
        "6",
        "9 18",
        "162"
    ),
    Problema(
        "Repara el rellotge",
        "Amb tanta obra, el dalendari digital de la entrada s'ha espatllat\n" +
                "Aprofitarem aquest error per posar un calendari que funcioni amb un programa que ens doni la data d'avui",
        "No hi ha entrada.",
        "Tornà la data d'avui amb el format aaa-mm-dd.",
        "",
        "2023-01-16",
        "(no hi ha input)",
        "${LocalDate.now()}"
    )
)
class Problema(val titol:String, val enunciat:String, val entrada:String, val sortida:String, val publicInput:String, val publicOutput:String, val privateInput:String, val privateOutput:String, ){
    var resolt:Boolean = false
    var intents:Int = 0
    val intents_usuari:MutableList<String> = mutableListOf()

    fun printQuestion(n:Int){
        println("\u001B[7m Programa ${n+1}: ${this.titol}\u001B[0m")
        println()
        println("\u001B[1m ${this.enunciat}")
        println()
        println(this.entrada)
        println("${this.sortida}\u001B[0m")
        println()
        println("Exemple:")
        println("     Input: ${this.publicInput}")
        println("     Output: ${this.publicOutput}")
        println()
        println("\u001B[3m El teu input és: ${this.privateInput}\u001B[0m")
        println()
    }

    fun anwerQuestions(){
        do{
            this.intents++
            print("Escriu aquí el teu output:   ")
            val userInput = sc.nextLine()
            if(userInput == this.privateOutput) {
                println()
                println("És correcte")
                this.resolt = true
            }
            else println("És incorrecte")
            this.intents_usuari.add(userInput)
            this.imprimirEstat()
        }while (!this.resolt)
    }

    fun imprimirEstat(){
        println()
        println("Quantitat d'intents: ${this.intents}")
        println("Els teus intents: ")
        for(i in this.intents_usuari.indices) println("   - ${this.intents_usuari[i]}")
        print("Estat: \u001B[92m")
        if(!this.resolt) print("\u001B[91m no ")
        println("resolt \u001B[0m")
        println()
    }
}


fun main(){

    var problemesResolts = 0

    for(i in problemes.indices){
        val problema = problemes[i]
        problema.printQuestion(i)
        println("\u001B[96m     Introdueix:\n" +
                "1 per a resoldre'l\n" +
                "2 per a no resoldre'l\u001B[0m")
        do{
            print("Escull una opció vàlida:   ")
            var userInput = sc.nextLine().toInt()
            if(userInput==1){
                println()
                problemesResolts++
                problema.anwerQuestions()
            }else if(userInput == 2) println()
            else userInput = 0
        }while (userInput==0)
    }

    println("Problemes resolts: $problemesResolts/${problemes.size}")
}
